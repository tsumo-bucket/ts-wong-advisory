<div class="caboodle-form-group sumo-asset-select" data-crop-url="{{route('admin{$ROUTE_NAME}CropUrl')}}">
  <label for="{$NAME}">{$PLACEHOLDER}</label>
  {!! Form::hidden('{$NAME}', null, ['class'=>'sumo-asset', 'data-id'=>@$data->id, 'data-thumbnail'=>'{$NAME}_thumbnail']) !!}
  <span class="sub-text-1">The required image size is 100x100 pixels minimum</span>
</div>
